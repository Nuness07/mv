var openMenu = false;
const body = document.querySelector('body');


// Abrir Menu
const btnOpenMenu = document.querySelector('.mv-btnOpenMenu');
const menuMobile = document.querySelector('.mv-menu-mobile');

btnOpenMenu.addEventListener('click', () => {
    // Abre e fecha menu quando o scroll estiver no topo
    if (btnOpenMenu.classList.contains('mv-btnActive')) {
        btnOpenMenu.classList.remove('mv-btnActive');
        menuMobile.classList.remove('mv-menu-mobile-show');
        openMenu = false;
        scrollTravado(openMenu);
    } else {
        btnOpenMenu.classList.add('mv-btnActive');
        menuMobile.classList.add('mv-menu-mobile-show');
        openMenu = true;
        scrollTravado(openMenu);
    }
})

//Verifica se o header está reduzido e reposiciona o menu
function animeMenu() {
    var distanceTop = window.pageYOffset;
    if (distanceTop > 50) {
        menuMobile.classList.add('mv-menu-mobile-header-reduzido');
    } else {
        menuMobile.classList.remove('mv-menu-mobile-header-reduzido');
    }
}

window.addEventListener('scroll', function () {
    animeMenu();
});

// Abrir menu search
const btnOpenSearchMenu = document.querySelector('.mv-btnOpenSearchMenu');
const btnCloseSearchMenu = document.querySelector('.mv-btnCloseSearchMobile');
const searchMobileMenu = document.querySelector('.mv-search-mobile-menu');

btnOpenSearchMenu.addEventListener('click', () => {
    searchMobileMenu.classList.add('mv-search-mobile-menu-show');
})

btnCloseSearchMenu.addEventListener('click', () => {
    searchMobileMenu.classList.remove('mv-search-mobile-menu-show');
})

// INICIO DO SCRIPT DO HEADER REDUZIDO(SCROLL)
const mvHeaderScroll = document.querySelector(' .mv-header');

// IMAGENS E ELEMENTOS QUE TROCAM DE COR NO HEADER DA HOME
var imagensWhite = document.querySelectorAll('.mv-home-header img');
var btnOpenMenuHome = document.querySelector('.mv-menu-burguer');



function animeHeader() {
    var distanceTop = window.pageYOffset;
    if (distanceTop > 50) {
        mvHeaderScroll.classList.add('mv-header-scroll');
        btnOpenMenuHome.classList.add('mv-menu-burguer-header-reduzido');
        searchMobileMenu.classList.add('mv-search-mobile-menu-reduzido');


        // Quando o menu da home mobile está reduzido as imagens ficam pretas
        imagensWhite.forEach((item) => {
            var srcBrancoImagem = item.src
            var srcPretoImagem = srcBrancoImagem.replace('white', 'black');
            item.src = srcPretoImagem;
        })
    } else {
        mvHeaderScroll.classList.remove('mv-header-scroll');
        btnOpenMenuHome.classList.remove('mv-menu-burguer-header-reduzido');
        searchMobileMenu.classList.remove('mv-search-mobile-menu-reduzido');
        // Quando o menu da home mobile está no topo ele fica transparente portanto
        // as imagens voltam a ficar brancas
        imagensWhite.forEach((item) => {
            var srcBrancoImagem = item.src
            var srcPretoImagem = srcBrancoImagem.replace('black', 'white');
            item.src = srcPretoImagem;
        })

    }
}

window.addEventListener('scroll', function () {
    animeHeader();
});

// FIM DO SCRIPT DO MENU REDUZIDO

/* TROCAR NOME DA COR */
var labelColor = document.querySelectorAll('.mv-radio-color label')
var titleColor = document.querySelector('.mv-radio-color .mv-title-color')
labelColor.forEach(item => {
    item.addEventListener('click', function changeColor(e) {
        const element = e.target;
        const titleColorAlt = element.children[0].getAttribute('alt');
        titleColor.innerText = titleColorAlt;
    });
})

/* CHECK TAMANHO */
var labelSize = document.querySelectorAll('.mv-radio-size label')
var inputSize = document.querySelectorAll('.mv-radio-size input')
function loadSizeRadio() {
    var checked = false;
    for (var i = 0; i < labelSize.length; i++) {
        if (!labelSize[i].classList.contains("mv-radio-size-disabled")) {
            if (checked === false) {
                inputSize[i].checked = true;
                checked = true;
            }
        } else {
            inputSize[i].disabled = true;
        }
    }
}

window.onload = function () {
    loadSizeRadio();
};

/* DROPDOWN */
var accordions = document.getElementsByClassName("mv-accordion");

for (var i = 0; i < accordions.length; i++) {
    accordions[i].onclick = function () {
        this.classList.toggle('is-open');

        var content = this.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.paddingBottom = '0px';
            content.style.paddingTop = '0px';
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + 25 + "px";
            content.style.paddingBottom = '25px';
        }
    }
}

if (!!document.querySelector('footer')) {
    // Abrir e fechar footer
    const btnOpenFooterAjuda = document.querySelector('.mv-footer-info-ajuda-mobile');
    const btnOpenFooterMv = document.querySelector('.mv-footer-info-mv-mobile');
    const footerAjuda = document.querySelector('.mv-footer-info-ajuda-mobile-links');
    const footerMv = document.querySelector('.mv-footer-info-mv-mobile-links');

    btnOpenFooterAjuda.addEventListener('click', () => {
        footerAjuda.classList.toggle('mv-footer-active');
        const srcImageFooter = btnOpenFooterAjuda.children[1].src;
        if (srcImageFooter.indexOf("open") == -1) {
            const openImage = srcImageFooter.replace('close', 'open');
            setTimeout(() => {
                btnOpenFooterAjuda.children[1].src = openImage;
            }, 300);

        } else {
            const closeImage = srcImageFooter.replace('open', 'close');
            btnOpenFooterAjuda.children[1].src = closeImage;
        }
    })

    btnOpenFooterMv.addEventListener('click', () => {
        footerMv.classList.toggle('mv-footer-active');
        const srcImageFooter = btnOpenFooterMv.children[1].src;
        if (srcImageFooter.indexOf("open") == -1) {
            const openImage = srcImageFooter.replace('close', 'open');
            setTimeout(() => {
                btnOpenFooterMv.children[1].src = openImage;
            }, 300);
        } else {
            const closeImage = srcImageFooter.replace('open', 'close');
            btnOpenFooterMv.children[1].src = closeImage;
        }
    })
}

// Modal cookie
if (document.querySelector('.mv-cookie')) {
    const cookieModal = document.querySelector('.mv-cookie');
    const btnCookie = document.querySelector('.mv-cookie .mv-btn-agree');
    if(localStorage.hideModal !== '.mv-cookie') {
        btnCookie.addEventListener('click', function (e) {
            e.preventDefault();
            cookieModal.classList.add('mv-disabled-fade');
            localStorage.hideModal = '.mv-cookie';
        });
    } else {
        cookieModal.classList.add('mv-disabled');
    }
}

if (document.querySelector('#mv-modal-cupom')) {
    modal('mv-modal-cupom');
} 

function modal(modalId) {
    if(localStorage.modalCupom !== modalId) {
        const modal = document.getElementById(modalId)
        modal.classList.add('mv-active');
        modal.addEventListener('click', (e) => {
            console.log(e.target.id)
            if(e.target.id === modalId || e.target.className === 'mv-btn-close-modal') {
                modal.classList.remove('mv-active');
                localStorage.modalCupom = modalId;
            }
        })
    }
}

if(document.querySelector('.mv-modal-container')) {
    const modalSectionOne = document.querySelector('.mv-modal-container .mv-modal-section-1');
    const modalSectionTwo = document.querySelector('.mv-modal-container .mv-modal-section-2');
    const inputNome= document.getElementById('mv-nome-modal');
    const inputEmail = document.getElementById('mv-email-modal');
    const btnCadastrar = document.querySelector('.mv-btn-modal-cadastrar');
    const btnVoltar = document.querySelector('.mv-btn-modal-voltar');

    btnCadastrar.addEventListener('click', (e) => {
        e.preventDefault();
        var valInputNome = inputNome.value;
        var valInputEmail = inputEmail.value;
        
        if(!(valInputNome === '' || valInputEmail === '')) {
            console.log(valInputNome)
            console.log(valInputEmail)
            modalSectionOne.classList.remove('mv-show');
            modalSectionTwo.classList.add('mv-show');
        }
    })

    btnVoltar.addEventListener('click', () => {
        inputNome.value = '';
        inputEmail.value = '';
        modalSectionOne.classList.add('mv-show');
        modalSectionTwo.classList.remove('mv-show');
    })
}




// Executar só na pg que tem as tendencias
if (!!document.querySelector('.mv-tendencias')) {
    const produtoTendencia = document.querySelectorAll('.mv-tendencia-produto');

    produtoTendencia.forEach((item) => {
        item.addEventListener('mouseover', () => {
            const hoverProduto = item.querySelector('a');
            hoverProduto.classList.add('mv-tendencia-hover-show');
        })
        item.addEventListener('mouseout', () => {
            const hoverProduto = item.querySelector('a');
            hoverProduto.classList.remove('mv-tendencia-hover-show');
        })
    })
}

// INICIO SCRIPT TROCAR visualização DE COLUNAS DA LISTAGEM DE PRODUTOS E SEARCH
var fourColumnsOn = document.querySelector('.mv-filter-quantity .mv-btn-four-columns');
var twoColumnsOn = document.querySelector('.mv-filter-quantity .mv-btn-two-columns');
var twoColumnsMobileOn = document.querySelector('.mv-btn-two-columns-mobile')
var oneColumnMobileOn = document.querySelector('.mv-btn-one-colum-mobile');

var listProducts = document.querySelector('.mv-list-products');
var produtoWrapper = document.querySelectorAll('.mv-produto-wrapper');

// No Mobile é para 1 coluna
function toggleFor2Columns() {
    fourColumnsOn.classList.add('mv-none');
    twoColumnsMobileOn.classList.add('mv-none');
    twoColumnsOn.classList.remove('mv-none');
    oneColumnMobileOn.classList.remove('mv-none');

    listProducts.classList.add('mv-two-columns-margin');

    produtoWrapper.forEach((item) => {
        item.classList.add('mv-two-columns');
        item.classList.remove('mv-four-columns');
    })
}

// No mobile é para 2 colunas
function toggleFor4Columns() {
    fourColumnsOn.classList.remove('mv-none');
    twoColumnsMobileOn.classList.remove('mv-none');
    twoColumnsOn.classList.add('mv-none');
    oneColumnMobileOn.classList.add('mv-none');

    listProducts.classList.remove('mv-two-columns-margin');

    produtoWrapper.forEach((item) => {
        item.classList.remove('mv-two-columns');
        item.classList.add('mv-four-columns');
    })
}

fourColumnsOn.addEventListener('click', function () {
    toggleFor2Columns()
})

oneColumnMobileOn.addEventListener('click', function () {
    toggleFor4Columns()
})

twoColumnsMobileOn.addEventListener('click', function () {
    toggleFor2Columns()
})

twoColumnsOn.addEventListener('click', function () {
    toggleFor4Columns()
})


// FILTROS
// Abrir filtro ordenar
function openOrdenar() {
    const filtroOrdenar = document.querySelector('.mv-filter-ordenar-options');
    const imgBtn = document.querySelector('.mv-filter-img');
    filtroOrdenar.classList.toggle('mv-filter-option-options-show');
    imgBtn.classList.toggle('mv-filter-btn-img-rotate');
}

const opcaoOrdenar = document.querySelector('.mv-filter-ordenar-btn');
const opcoesOrdenar = document.querySelectorAll('.input-radio input');
opcoesOrdenar.forEach((item) => {
    item.addEventListener('click', () => {
        if (item.checked) {
            var nomeItem = item.labels[0].innerHTML;
            opcaoOrdenar.children[0].innerHTML = nomeItem;
        }
    })
})


// Abrir restante dos filtros


// function filtrosFechar(){
//     var filtros = document.querySelectorAll('.mv-filter-option-options');
//     filtros.forEach((item) => {
//         item.classList.remove('mv-filter-option-options-show');
//     })
// }

// btnOpenFilter.forEach((item) => {
//     item.addEventListener('click', () => {
//         // filtrosFechar();
//         // Abre os outros filtros
//         var id = item.innerText;
//         item.children[1].classList.toggle('mv-filter-btn-img-rotate')
//         var filterToOpen = document.getElementById(id);
//         filterToOpen.classList.toggle('mv-filter-option-options-show');
//     })
// })

/* Abrir filtros desktop */
const btnOpenFilter = document.querySelectorAll('.mv-filter-btn');
const poupItens = document.querySelectorAll('.mv-filter-option-options');
const btnNovidades = document.querySelector('.mv-filter-novidade')
var showVeri = '';
btnOpenFilter.forEach((item) => {
    var classRotateImg = 'mv-filter-btn-img-rotate';
    var classShowOptions = 'mv-filter-option-options-show';
    
    item.addEventListener('click', () => {
        var id = item.innerText; // Pega ID do item clicado
        var filterToOpen = document.getElementById(id);

        for(var i = 0; i < poupItens.length; i++) {
            poupItens[i].classList.remove(classShowOptions);
        }

        for(var i = 0; i < btnOpenFilter.length; i++) {
            btnOpenFilter[i].children[1].classList.remove(classRotateImg);
        }

        btnNovidades.children[1].classList.remove(classRotateImg);

        if(item.children[1].classList.contains(classRotateImg)) {
            item.children[1].classList.remove(classRotateImg);
        } else {
            item.children[1].classList.add(classRotateImg);
        }
        
        if(filterToOpen.classList.contains(classShowOptions)) {
            filterToOpen.classList.remove(classShowOptions);
        } else {
            filterToOpen.classList.add(classShowOptions);
        }

        if(showVeri === id) {
            filterToOpen.classList.remove(classShowOptions);
            item.children[1].classList.remove(classRotateImg);
            showVeri = '';
        } else {
            showVeri = id;
        }
    })
})

// Abrir Ordenar Mobile
const btnOpenOrdenarMobile = document.querySelector('.mv-filter-ordenar-mobile');
const btnCloseOrdenarMobile = document.querySelector('.mv-btn-aplicar-ordenar');
const ordenarMobile = document.querySelector('.mv-filter-ordenar-container-mobile');

btnOpenOrdenarMobile.addEventListener('click', () => {
    ordenarMobile.classList.add('mv-filter-container-mobile-show');
    openMenu = true;
    scrollTravado(openMenu);
})

// Clica no botão de aplicar e fecha o filtro
btnCloseOrdenarMobile.addEventListener('click', () => {
    ordenarMobile.classList.remove('mv-filter-container-mobile-show');
    openMenu = false;
    scrollTravado(openMenu);
})

// Abrir filtrar mobile
const btnOpenFiltrarMobile = document.querySelector('.mv-filter-filtrar-mobile');
const btnCloseFiltrarMobile = document.querySelector('.mv-btn-aplicar-filtros');
const filtrosMobile = document.querySelector('.mv-filter-options-container-mobile');

btnOpenFiltrarMobile.addEventListener('click', () => {
    filtrosMobile.classList.add('mv-filter-container-mobile-show');
    openMenu = true;
    scrollTravado(openMenu);
})

// Clica no botão de aplicar e fecha o filtro
btnCloseFiltrarMobile.addEventListener('click', () => {
    filtrosMobile.classList.remove('mv-filter-container-mobile-show');
    openMenu = false;
    scrollTravado(openMenu);
})




function scrollTravado(menuOpen) {
    if (menuOpen) {
        body.style.overflowY = 'hidden';
    } else {
        body.style.overflowY = 'scroll';
    }
}