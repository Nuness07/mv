var swiperHeader = new Swiper('.swiper-container.mv-swiper-photos', {
    slidesPerView: 1,
    spaceBetween: 16,
    centeredSlides: true,
    pagination: {
        el: '.swiper-pagination.mv-pagination-swiper-photos',
    },
    navigation: {
        nextEl: '.swiper-button-next.mv-swiper-button-next-swiper-header',
        prevEl: '.swiper-button-prev.mv-swiper-button-prev-swiper-header',
    },
});

var swiperCompleteLook = new Swiper('.swiper-container.mv-swiper-container-complete-look', {
  slidesPerView: 1,
  spaceBetween: 16,
  breakpoints:{
      440: {
          slidesPerView: 2,
          spaceBetween: 16,
          centeredSlides: false,
      },
      768: {
          slidesPerView: 3,
          spaceBetween: 16,
          centeredSlides: false,
      },
      992: {
          slidesPerView: 3,
          spaceBetween: 16,
          centeredSlides: false,
      },
      1200: {
          slidesPerView: 4,
          spaceBetween: 30,
          centeredSlides: false,
      },
  }
});

var swiperLike = new Swiper('.swiper-container.mv-swiper-container-you-like', {
  slidesPerView: 1,
  spaceBetween: 16,
  breakpoints:{
      440: {
          slidesPerView: 2,
          spaceBetween: 16,
          centeredSlides: false,
      },
      768: {
          slidesPerView: 3,
          spaceBetween: 16,
          centeredSlides: false,
      },
      992: {
          slidesPerView: 3,
          spaceBetween: 16,
          centeredSlides: false,
      },
      1200: {
          slidesPerView: 4,
          spaceBetween: 30,
          centeredSlides: false,
      },
  }
});

